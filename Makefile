LIBOBJS += $(wildcard ../../O.${T_A}/lib/.libs/*.o)

include ${EPICS_ENV_PATH}/module.Makefile

PROJECT=lmfit
SRCPATH=lmfit-6.1

SOURCES = -none-

#HEADERS_PREFIX = 
HEADERS = $(wildcard ${SRCPATH}/lib/*.h)
