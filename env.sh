# File to download and unpack.
TARBALL=http://apps.jcns.fz-juelich.de/src/lmfit/lmfit-6.1.tgz
# Path where source tarball was unpacked.
SRCPATH=lmfit-6.1

PACKAGE_CONFIGURE_FLAGS="--with-pic"
